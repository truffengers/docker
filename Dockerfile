# node > 14.6.0 is required for the SFDX-Git-Delta plugin
FROM jfgarcia268/sfdx_vlocity_java:latest

RUN npm cache clean -f
RUN npm i -g n
RUN n latest

RUN sfdx update

# install SFDX-Git-Delta plugin - https://github.com/scolladon/sfdx-git-delta
RUN echo y | sfdx plugins:install sfdx-git-delta
RUN sfdx plugin

# install jq
RUN apt-get install -y jq